|==============================================================================================================================================================================================================
|Description |Obligation |Comment

|Group attributes | |
|Dimensions | |
|time | |can be fixed or unlimited length, as appropriate

|Variables | |
|float surface_sound_speed(time) |M |surface sound speed in water
|float :valid_min = 0.0 | |
|:standard_name = "speed_of_sound_in_sea_water" | |
|:units = "m/s" | |
|:long_name = "Indicative mean sound speed" | |
| | |
|ulong time(time) |M |Time of measurement
|:calendar = "gregorian" | |
|:standard_name = "time" | |
|:units = "nanoseconds since 1970-01-01 00:00:00Z" | |
|:axis = "T" | |
|:long_name = "Timestamps for svp data" | |

|==============================================================================================================================================================================================================
