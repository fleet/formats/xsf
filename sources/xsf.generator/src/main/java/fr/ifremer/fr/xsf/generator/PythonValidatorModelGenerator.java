package fr.ifremer.fr.xsf.generator;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.fr.xsf.generator.model.Group;
import fr.ifremer.fr.xsf.generator.model.SCGroupAdapter;

public class PythonValidatorModelGenerator {

	private static String OUTPUT_FOLDER_API = "python";
	
	/**
	 * generate documentation in excel file format
	 * @throws Exception 
	 **/
	public void run(Group root, File outputDir) throws Exception
	{
		generateSourceFile(Paths.get(outputDir.getAbsolutePath(),OUTPUT_FOLDER_API).toFile(),  root);
	}

	
	/**
	 * Generates source files
	 * 
	 * @throws URISyntaxException
	 */
	private void generateSourceFile(File outputDir, Group group)
			throws IOException {

		if (!outputDir.exists())
			Files.createDirectories(outputDir.toPath());
		Path outputFolder=outputDir.toPath();
		Files.write(outputFolder.resolve(Paths.get("sonar_validator_model.py")),
					SCGroupAdapter.getValidatorSourceCode(group,"",""));
	}
	
}
