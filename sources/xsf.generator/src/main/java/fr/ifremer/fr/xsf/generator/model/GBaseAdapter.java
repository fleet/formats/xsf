package fr.ifremer.fr.xsf.generator.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

public class GBaseAdapter {

	public static JsonObject serialize(JsonObject object,Base value,JsonSerializationContext context )
	{
		object.add("name", context.serialize(value.name));
		object.add("description", context.serialize(value.description));
		object.add("obligation", context.serialize(value.obligation));
		object.add("comment", context.serialize(value.comment));
		return object;	
	}
}
