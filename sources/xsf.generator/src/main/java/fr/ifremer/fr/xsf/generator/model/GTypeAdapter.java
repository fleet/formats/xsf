package fr.ifremer.fr.xsf.generator.model;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GTypeAdapter  implements JsonSerializer< fr.ifremer.fr.xsf.generator.model.Type> {

	@Override
	public JsonElement serialize( fr.ifremer.fr.xsf.generator.model.Type value, Type type, JsonSerializationContext context) {
		JsonObject object = new JsonObject();
		GBaseAdapter.serialize(object, value, context);
		
		return object;
		
	}

}
