package fr.ifremer.fr.xsf.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.ifremer.fr.xsf.generator.model.Attribute;
import fr.ifremer.fr.xsf.generator.model.GAttributeAdapter;
import fr.ifremer.fr.xsf.generator.model.GGroupAdapter;
import fr.ifremer.fr.xsf.generator.model.GTypeAdapter;
import fr.ifremer.fr.xsf.generator.model.GVariableAdapter;
import fr.ifremer.fr.xsf.generator.model.Group;
import fr.ifremer.fr.xsf.generator.model.Type;
import fr.ifremer.fr.xsf.generator.model.Variable;

public class XSFCodeGenerator {

	static File outputDir=new File("C:\\data\\tmp\\XSFGenerator");

	private static final String OUTPUT_DIR_OPTION="out";
	private static final String MODEL_FILE_OPTION="model";
	private static final String HELP_OPTION = "h";

	public static Gson getGson()
	{
		return new GsonBuilder().registerTypeAdapter(Group.class,new GGroupAdapter())
				.registerTypeAdapter(Type.class,new GTypeAdapter())
				.registerTypeAdapter(Variable.class,new GVariableAdapter())
				.registerTypeAdapter(Attribute.class,new GAttributeAdapter())
				.setPrettyPrinting().create();
	}
	public static void main( String[] args ) throws Exception
	{

		// Parse file
		CommandLine commandLine = new DefaultParser().parse(createCommandLineOptions(), args);
		if (commandLine.hasOption(HELP_OPTION)) { // Display help if asked
			displayHelp();
			return;
		} else {
			outputDir=new File(commandLine.getOptionValue(OUTPUT_DIR_OPTION));
			if(!outputDir.isDirectory())
			{
				System.err.println(String.format("Error %s is not a directory or does not exist",outputDir.toString()));
				displayHelp();
				return;
			}

		} 
		System.out.println("Starting into directory " + outputDir.getAbsolutePath() );
		String currentDir = System.getProperty("user.dir");
		System.out.println("Current dir using System:" + currentDir);
	   
		{
			Group root=readJson("./model/xsf/Root.json");
			Group annotation=readJson("./model/xsf/Annotation.json");
			Group environment=readJson("./model/xsf/Environment.json");
			Group platform=readJson("./model/xsf/Platform.json");
			Group provenance=readJson("./model/xsf/Provenance.json");
			Group sonar=readJson("./model/xsf/Sonar.json");

			root.getSubGroups().addAll(Arrays.asList(annotation,environment,platform,provenance,sonar));
			new AsciiDoctorGenerator().run(root, Paths.get(outputDir.getAbsolutePath()).toFile());
			new JavaSourceCodeGenerator().run(root, Paths.get(outputDir.getAbsolutePath(), "code").toFile());
			new PythonSourceCodeGenerator().run(root, Paths.get(outputDir.getAbsolutePath(), "code").toFile(),"sonar_groups.py");
			new PythonValidatorModelGenerator().run(root, Paths.get(outputDir.getAbsolutePath(), "code").toFile());
		}
		System.out.println("Done ");

	}

	public static Group readJson(String file_name) throws FileNotFoundException, IOException
	{
		try (Reader reader=new InputStreamReader(new FileInputStream(file_name),StandardCharsets.UTF_8)){
			Gson gson=getGson();
			Group root=gson.fromJson(reader, Group.class);
			return root;
		}
	}

	/** Display help. */
	public static void displayHelp() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("CodeGenerator", createCommandLineOptions());
	}

	/**
	 * Creates command line options.
	 */
	protected static Options createCommandLineOptions() {
		Options result = new Options();
		result.addOption(Option.builder(OUTPUT_DIR_OPTION).required(true).hasArg(true)
				.desc("output directory").build());
		result.addOption(Option.builder(MODEL_FILE_OPTION).required(false).hasArg(true)
				.desc("model description as a json file ").build());
		return result;		
	}

}
