package fr.ifremer.fr.xsf.generator.model;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class SCAttributeAdapter {
	public static List<String> getSourceDeclaration(Attribute attribute) {
		return Arrays.asList(
				String.format("\tpublic static final String %s=\"%s\";", "ATT_"+attribute.name.toUpperCase(),attribute.name));
	}

	public static List<String> getSourceCode(Group group, Attribute attribute) {
		if(attribute.getType()==null)
		{
			//treat as String
			return Arrays.asList(String.format("addAttribute(%s, \"%s\");","ATT_"+attribute.name.toUpperCase(), attribute.value == null ? "":attribute.value.replace("+","")));
		}
		return Arrays.asList(String.format("addAttribute(%s, %s);","ATT_"+attribute.name.toUpperCase(), formatAttribute(attribute,attribute.getType())));
	}
	public static List<String> getSourceCode(Variable variable, Attribute attribute) {
		
		String type = variable.getType();
		if (attribute.getType() != null)
		{
			//override type attribute 
			type = attribute.getType();
		}	
		
		if(attribute.getName().compareTo("_FillValue")==0){
			switch(type)
			{
			case "float":
				return Arrays.asList(String.format("setFloatFillValue(%s);",formatAttribute(attribute, type)));
			case "double":
				return Arrays.asList(String.format("setDoubleFillValue(%s);",formatAttribute(attribute, type)));
			case "byte":
			case "ubyte":
				return Arrays.asList(String.format("setByteFillValue(%s);",formatAttribute(attribute, type)));
			case "short":
			case "ushort":
				return Arrays.asList(String.format("setShortFillValue(%s);",formatAttribute(attribute, type)));
			case "int":
			case "uint":
				return Arrays.asList(String.format("setIntFillValue(%s);",formatAttribute(attribute, type)));
			case "long":
			case "ulong":
				return Arrays.asList(String.format("setLongFillValue(%s);",formatAttribute(attribute, type)));
			default:
				throw new RuntimeException("Unsupported fill value type " + type );
			}
			
			// retrieve the fill value
		} else if(attribute.getName().compareTo("valid_range")==0) {
			switch(type)
			{
			case "float":
			case "double":
			case "byte":
			case "short":
			case "int":
			case "long":
				return Arrays.asList(String.format("addAttribute(\"%s\", new %s[] {%s});",attribute.name, type,attribute.value));
			case "ubyte":
			case "ushort":
			case "uint":
			case "ulong":
				throw new RuntimeException("Unsupported valid_range value type " + type);
			default:
				return getSourceCode(attribute,s->s);
			}
			
		} else if(attribute.getName().compareTo("valid_min")==0 || attribute.getName().compareTo("valid_max")==0 || attribute.getName().compareTo("missing_value")==0) {
			switch(type)
			{
			case "float":
			case "double":
			case "byte":
			case "short":
			case "int":
			case "long":
				return Arrays.asList(String.format("addAttribute(\"%s\", %s);",attribute.name, formatAttribute(attribute,type)));
			case "ubyte":
			case "ushort":
			case "uint":
			case "ulong":
				return Arrays.asList(String.format("addUnsignedAttribute(\"%s\", %s);",attribute.name, formatAttribute(attribute,type)));
			default:
				return Arrays.asList(String.format("addAttribute(\"%s\", %s);",attribute.name, formatAttribute(attribute,type)));
			}
		} else {
			if(attribute.getType()==null)
			{
				//treat as String
				return getSourceCode(attribute, s-> s.replace("+",""));
			}
			return Arrays.asList(String.format("addAttribute(\"%s\", %s);",attribute.name, formatAttribute(attribute,attribute.getType())));
			
		}
	}
	private static String formatAttribute(Attribute attribute, String expectedType)
	{
		switch(expectedType)
		{
		case "float":
			return String.format("(float) %s",attribute.value == null ? "0f":attribute.value);
		case "double":
			return String.format("(double) %s", attribute.value == null ? "0":attribute.value);
		case "short":
		case "ushort":
			return String.format("(short) %s",attribute.value == null ? "0":attribute.value);
		case "ubyte":
		case "byte":
			return String.format("(byte) %s",attribute.value == null ? "0":attribute.value);
		case "uint":
		case "int":
			return String.format("(int) %s",attribute.value == null ? "0" :attribute.value);
		case "ulong":
		case "long":
			return String.format("(long) %s",attribute.value == null ? "0": attribute.value);

		default:
			return String.format("\"%s\"",attribute.value == null ? "":attribute.value);
		}
	}
	
	public static List<String> getSourceCode(Attribute attribute, Function<String,String> preformater) {

		return Arrays.asList(String.format("addAttribute(\"%s\", \"%s\");",attribute.name, attribute.value == null ? "":preformater.apply(attribute.value)));
	}

	
}
