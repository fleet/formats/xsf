package fr.ifremer.fr.xsf.generator.model;

import java.util.Arrays;
import java.util.List;

public class SCDimensionAdapter {

	/**
	 * return netcdf variable instanciation
	 * */
	public static List<String> getSourceCode(Dimension dimension) {
		
		String size = dimension.javaValue == null ? "0" : dimension.javaValue;
		return Arrays.asList(
				String.format("\t\t%s = declareDimension(%s, %s); //%s", getDimCodeName(dimension.name), getDimNameDeclaration(dimension), size, dimension.comment==null ? "" : dimension.comment));
	}
	
	public static List<String> getSourceAccessor(Dimension dim) {
		String name=getDimCodeName(dim.name);
		return Arrays.asList(String.format("\t/**\n"
				+ "\t * @return the %s\n"
				+ "\t */\n"
				+ "\tpublic NCDimension get%s() {\n"
				+ "\t\treturn %s;"
				+ "\n\t}\n", name,CamelCase(name),name));
	}
	private static String CamelCase(String name)
	{
		return name.substring(0, 1).toUpperCase()+name.substring(1);
	}
	
	/**
	 * return the variable declaration
	 * */
	public static String getSourceDeclaration(Dimension dimension) {
		return String.format("\tprivate NCDimension %s;", getDimCodeName(dimension.name));
	}

	/** 
	 * return dimension variable name
	 * */
	public static String getDimCodeName(String dimension)
	{	
	//	return endsWithDim(dimension.name) ? dimension.name : dimension.name+"Dim";
		return "dim_"+dimension;
	}
		

	/** 
	 * return dimension name in netcdf
	 * */
	public static String getDimNCName(Dimension dimension)
	{	
		return dimension.name;
	}
		
	
	/**
	 * return dimension name declaration ie the constant string used to store name value
	 * */
	public static String getDimNameDeclaration(Dimension dimension)
	{
		return dimension.name.toUpperCase() + "_DIM_NAME";
	}
}
