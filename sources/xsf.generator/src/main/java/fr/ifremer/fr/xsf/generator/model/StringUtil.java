package fr.ifremer.fr.xsf.generator.model;

public class StringUtil {

	public static String clean(String value) {
		if (value == null)
			return "";
		return value.trim().replace("\u00A0", "");
	}

}
