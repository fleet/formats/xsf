package fr.ifremer.fr.xsf.generator.model;

import java.util.ArrayList;
import java.util.List;


public class Variable extends Base{
	final String type;
	public Variable(String name, String description, String comment, String obligation, String type) {
		super(name, description, comment, obligation);
		if(type==null || type.isEmpty())
		{
			this.type=null;
		} else
		{
			this.type = type;

		}
	}
	public String getType() {
		return type;
	}
	public List<String> getDims() {
		return dims;
	}
	public List<Attribute> getAttributes() {
		return attributes;
	}
	List<String> dims = new ArrayList<>();
	List<Attribute> attributes = new ArrayList<>();
	public void postRead() {
		// attributes declarations
		ArrayList<Attribute> toAdd=new ArrayList<>();
		boolean foundFillValue=	attributes.stream().anyMatch(att -> att.getName().compareTo("_FillValue")==0);
		if(!foundFillValue)
		{
			//add default _FillValue
			switch(type)
			{
			case "float":
				attributes.add(new Attribute("_FillValue", "", "", "", "Float.NaN", "float",null));
				break;
			case "double":
				attributes.add(new Attribute("_FillValue", "", "", "", "Double.NaN", "double",null));
				break;
			}
		}
		
	}

}
