package fr.ifremer.fr.xsf.generator;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore.Entry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.ifremer.fr.xsf.generator.model.Group;
import fr.ifremer.fr.xsf.generator.model.SCGroupAdapter;

public class JavaSourceCodeGenerator {
	private static String OUTPUT_PACKAGE = "fr.ifremer.globe.api.xsf.converter.common.xsf.groups";

	private static String OUTPUT_FOLDER_PREDEFINED_LAYER = "predefinedlayers";
	private static String OUTPUT_FOLDER_API = "api";
	
	private static String VENDOR_SPECIFIC = "Vendor_specific";
	
	/**
	 * generate documentation in excel file format
	 * @throws Exception 
	 **/
	public void run(Group root, File outputDir) throws Exception
	{
		Path outputPath=Paths.get(outputDir.getAbsolutePath(), OUTPUT_FOLDER_PREDEFINED_LAYER);
		generateSourceFile(Paths.get(outputDir.getAbsolutePath(),OUTPUT_FOLDER_API).toFile(), Arrays.asList(root));
		new PredefinedLayersGenerator().generateSourceFile(root.getSubGroups(),outputPath);
	}

	
	/**
	 * Generates source files
	 * 
	 * @throws URISyntaxException
	 */
	private void generateSourceFile(File outputDir, List<Group> groups)
			throws IOException, URISyntaxException {

		Map<String, Group> finalGroups = new TreeMap<>();
		groups.forEach(g -> treeWalker(g, finalGroups, "//"));
		


		if (!outputDir.exists()) {
			Files.createDirectories(outputDir.toPath());
		}
		
		File vendorSpecificDir = new File(outputDir, "vendor_specific");
		if (!vendorSpecificDir.exists()) {
			Files.createDirectories(vendorSpecificDir.toPath());
		}
		
		Path outputFolder = outputDir.toPath();
		Path specificFolder = vendorSpecificDir.toPath();
		
		for (Map.Entry<String, Group> entry : finalGroups.entrySet()) {
			String groupPath = entry.getKey();
			boolean isSpecific = groupPath.contains(VENDOR_SPECIFIC);
			Path folder = isSpecific ? specificFolder : outputFolder; 
					
			Path p = folder.resolve(Paths.get(String.format("%s.java", SCGroupAdapter.getClassName(entry.getValue(), entry.getKey()))));
			List<String> sourceCode = SCGroupAdapter.getSourceCode(entry.getKey(), entry.getValue(), OUTPUT_PACKAGE, isSpecific);
			
			Files.write(p, sourceCode);
		}
			
	}
	void treeWalker(Group g, Map<String, Group> map, String path)
	{
		String groupPath = path + g.getName();
		map.put(groupPath, g);
		g.getSubGroups().forEach(group->treeWalker(group, map, groupPath + "/"));
	}

	
}
