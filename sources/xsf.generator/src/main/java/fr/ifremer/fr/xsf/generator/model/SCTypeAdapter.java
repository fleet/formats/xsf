package fr.ifremer.fr.xsf.generator.model;

import java.util.Arrays;
import java.util.List;

import fr.ifremer.fr.xsf.generator.DataType;
import fr.ifremer.fr.xsf.generator.DataTypeConverter;

public class SCTypeAdapter {
	public static List<String> getSourceCode( String name) {
		try {
			if(DataTypeConverter.isVlen(name))
			{
				DataType type;
				type = DataTypeConverter.getVlenDataType(name);
				return 	Arrays.asList(String.format("\t\tthis.addType(\"%s\",typeDictionnary.getOrDefault(\"%s\",Nc4prototypes.%s)); //vlen for type %s",name,name,type.getNc_type_name(),type));
			} else if (name.contains("enum"))
			{
				return parseEnum(name);
			}
			
			return Arrays.asList(String.format("\t\t// %s", name));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static List<String> parseEnum(String name) {
		if(name.startsWith("byte enum"))
		{
			name=name.replace("\u00A0", " ");
			String a=name.replace("byte enum", "");
			a=a.trim();
			String spaced[]=a.split("\\{");
			String enum_name=spaced[0].trim();
			String values=spaced[1].replace("}","").trim();
			String value_list[]=values.split(",");
			StringBuilder builder=new StringBuilder();
			builder.append("\t\tthis.addEnumByte(\"");
			builder.append(enum_name);
			builder.append("\",Arrays.asList(");
			boolean first=true;
			for(String v: value_list)
			{
				String tup[]=v.split("=");
				String enum_value_name=tup[0].replace("\'","").replace(";","").trim();
				String enum_value=tup[1].replace("\'","").replace(";","").trim();
				if(!first)
					builder.append(",");
				builder.append("new Pair<Byte, String>((byte)");
				builder.append(enum_value);
				builder.append(",\"");
				builder.append(enum_value_name);
				builder.append("\")");
				first=false;
			}
			builder.append("));");
			return Arrays.asList(builder.toString());
		}
		else
			throw new RuntimeException("Not Supported enum type");
		
	}

}
