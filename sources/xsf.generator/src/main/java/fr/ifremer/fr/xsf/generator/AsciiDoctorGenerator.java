package fr.ifremer.fr.xsf.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.fr.xsf.generator.model.Attribute;
import fr.ifremer.fr.xsf.generator.model.Dimension;
import fr.ifremer.fr.xsf.generator.model.Group;
import fr.ifremer.fr.xsf.generator.model.Type;
import fr.ifremer.fr.xsf.generator.model.Variable;


public class AsciiDoctorGenerator {
	
	class Context {
		FileWriter currentOutput;
		//represents a generic asciidoc containing references
		FileWriter globalOutput;

		public File outputDir;
		public File currentOutputFileName;

		Context()
		{
		}
	}

	/**
	 * write a carriage return
	 * 
	 * @throws IOException
	 */
	public static void creturn(FileWriter out) throws IOException {
		out.write(System.lineSeparator());

	}

	public static void writeLine(String message,FileWriter out) throws IOException {
		if(message==null)
			message="";
		out.write(message);
		creturn(out);

	}

	public static void write(String message,FileWriter out) throws IOException {
		out.write(formatString(message));
	}

	public static String formatString(String value) {
		if (value == null)
			return "";
		return value.replace("\\n", System.lineSeparator());
	}
	/**
	 * Generate group documentation in asciidoctor format
	 * 
	 * @throws IOException
	 */
	public void run(Group root, File outputDir) throws IOException {
		Context context = new Context();


		File outputDirectory = new File(outputDir, "documentation");
		if (!outputDirectory.exists()) {
			Files.createDirectories(outputDirectory.toPath());
		}
		
		context.outputDir = outputDirectory;
		
		try(FileWriter gngOut = new FileWriter(Paths.get(context.outputDir.getAbsolutePath(),"Groups.adoc").toFile(),Charset.forName("UTF-8")))
		{
			context.globalOutput=gngOut;
			writeGroup("===","", root, context);
			
		}

	}
	static String CamelCase(String name)
	{
		if(name.length()==0) return name;
		return name.substring(0, 1).toUpperCase()+name.substring(1);
	}
	private String computeAdocName(Group group, String path)
	{
		if(group.getName().length()==0) return "tableTopLevel.adoc";
		if(group.getName().compareTo("Beam_group1")==0) return "tableBeamGroup1.adoc";
		if(group.getName().compareTo("Grid_group1")==0) return "tableBeamGridGroup1.adoc";
		if(group.getName().compareTo("ADCP")==0) return "tableADCP_sub_group.adoc";
		if(group.getName().compareTo("Mean_current")==0) return "tableMean_current_sub_group.adoc";
		
		if (group.getName().endsWith("Vendor_specific") || path.contains("Vendor_specific")) {
			String pathPrefix = path.substring(2).replace("/", "_") + "_";
			
			return "table" + pathPrefix + CamelCase(group.getName()) + ".adoc";
		}

		return "table" + CamelCase(group.getName()) + ".adoc";
	}
	private File computeFileName(File outputDir, Group group, String path)
	{
		return Paths.get(outputDir.getAbsolutePath(),computeAdocName(group, path)).toFile();
	}

	private void writeGroup(String level,String path, Group group, Context context) throws IOException {
		context.currentOutputFileName=computeFileName(context.outputDir, group, path);
		System.out.println(context.currentOutputFileName);
		try(FileWriter fileOut = new FileWriter(context.currentOutputFileName,Charset.forName("UTF-8")))
		{
			context.currentOutput=fileOut;
			// Create a Sheet

			// Write Group name
			String str=level+ " " +group.getName() +" (" +path +"/"+group.getName()+ ")";
			writeLine(str.replace("//", "/"),context.globalOutput);
			creturn(context.globalOutput);

			// Write group description
			writeLine(group.getDescription(),context.globalOutput);

			// Write group comments
			writeLine(group.getComment(),context.globalOutput);
			writeLine("include::"+computeAdocName(group, path)+"[]",context.globalOutput);
			
			startTable(context);
			if (!group.getAttributes().isEmpty())
				writeGroupAttributes(group.getAttributes(), context);
			writeTypes(group.getTypes(), context);
			writeDimensions( group.getDims(), context);
			writeCoordinateVariables( group.getCoordinateVariables(), context);
			writeVariables( group.getVariables(), context);
			endTable(context);
		}
		for (Group subgroup : group.getSubGroups()) {
			writeGroup(level,path+"/"+ group.getName(), subgroup, context);
		}
	}

	private void writeTypes(List<Type> types, Context context) throws IOException {
		if(types.isEmpty())
			return;
		// Create header for types
		writeSubHeader("Types", context);
		// write types
		for (Type v : types) {
			writeLine(" 2+|{var}" + v.getName() + " |" + v.getComment(),context.currentOutput);

		}
		creturn(context.currentOutput);
	}
	private void writeDimensions(List<Dimension> dims, Context context) throws IOException {
		if(dims.isEmpty())
			return;
		writeSubHeader("Dimensions", context);
		for(Dimension v:dims)
		{
			String value = v.getName();
			if(v.value!=null && v.value.length()>0)
				value=value+" = "+v.value;
			writeTableLine( " ","{var}"+value, v.getObligation(), v.getComment(), context);
		}
		writeLine(" ",context.currentOutput);
	}


	private void writeGroupAttributes(List<Attribute> attributes, Context context) throws IOException {
		
		writeSubHeader( "Group attributes", context);
		for(Attribute v:attributes)
		{
			String value = v.getType()!=null ? v.getType()+" ":"";
			value+=":";
			value+=v.getName();
			//assume a string type
			if(v.getType()==null|| v.getType().compareToIgnoreCase("string")==0)
			{
				if(v.getValue()!=null)
				{
					//on utilise des espace insécables pour se conformer à la doc WCFAST
					value+="\u00A0=\u00A0\"";
					value+=v.getValue().replace("+", "+\r\n");
					value+="\"";
				}
				
			}
			writeTableLine(" ","{attr}"+value, v.getObligation(), v.getComment(), context);

		}
		writeLine(" ", context.currentOutput);
	}
	private void writeCoordinateVariables(List<Variable> variables, Context context) throws IOException {
		if(variables==null || variables.isEmpty())
			return;
		reallyWriteVariable("Coordinate variables", variables, context);
		writeLine(" ", context.currentOutput);

	}
	private void reallyWriteVariable(String header,List<Variable> variables, Context context) throws IOException {

		writeSubHeader( header, context);
		boolean first=true;
		for(Variable v:variables)
		{
			//leave an empty line between two variables
			if(!first)
			{
				writeLine(" ", context.currentOutput);
			}
			first=false;

			// concatenate type name to variable name
			String value = v.getType()!=null ? v.getType():"";
			value+=" ";
			value+=v.getName();
			String dims=v.getDims().toString();
			dims=dims.replace("Dim", "");
			if(dims.compareTo("[]")!=0)
				value+=dims.replace('[','(').replace(']', ')');

			// really write
			writeTableLine(" ","{var}"+value, v.getObligation(), v.getComment(), context);
			//write sub attributes
			writeVariableAttributes(v.getAttributes(),context);
			//add empty line
			//writeTableLine("","", "", context);
		}
	}
	private void writeVariables(List<Variable> variables, Context context) throws IOException {
		if(variables.isEmpty())
			return;
		reallyWriteVariable("Variables", variables, context);
	}
	
	
	private void writeVariableAttributes( List<Attribute> attributes, Context context) throws IOException {
		for(Attribute v:attributes)
		{
			if(v.getValue()!=null && v.getValue().compareTo("Float.NaN")==0)
				continue;
			if(v.getValue()!=null && v.getValue().compareTo("Double.NaN")==0)
				continue;
			String value = v.getType()!=null ? v.getType().trim()+" ":"";
			value+=":";
			value+=v.getName();
			//assume a string type
			if(v.getType()==null|| v.getType().compareToIgnoreCase("string")==0)
			{
				if (v.getValues()!=null && v.getValues().length>0)
				{
					//we generate doc values by writing each possibility with a "or" between them
					StringBuilder b=new StringBuilder();
					value+="\u00A0=\u00A0";
					ArrayList<String> valueList=new ArrayList<>();
					for (String subv:v.getValues())
						valueList.add("\""+subv+"\"");

					value+=String.join(" or ",valueList);
					
				} else if(v.getValue()!=null)
				{
					//on utilise des espace insécables pour se conformer à la doc WCFAST
					value+="\u00A0=\u00A0\"";
					value+=v.getValue().replace("+", "+\r\n");
					value+="\"";
				}
			} else
			{
				if(v.getValue()!=null)
				{
					String lValue=v.getValue();
					if(!context.currentOutputFileName.getName().contains("Mean_current_sub_group") &&
							!context.currentOutputFileName.getName().contains("ADCP_sub_group") )
					{
						lValue=v.getValue().replaceAll("\u002D", "\u2212"); //remove minus-hyphen by minus (UTF8 encoding issue, two - sign exists
					} 
					
					//on utilise des espace insécables pour se conformer à la doc WCFAST
					value+="\u00A0=\u00A0";
					//remove hyphen minus U002D sign with minus sign U2212
					if(v.getName().compareTo("valid_range")==0)
					{
						value+=lValue.replaceAll("f", "");
						
					} else
					{
						value+=lValue.replaceAll(".0f", ".0");
						
					}
				}
			}
			if(v.getComment() != null && !v.getComment().isEmpty())
			{
				if (v.getObligation() != null && !v.getObligation().isEmpty() )
				{
					writeTableLine(" ","{attr}"+value,v.getObligation(),v.getComment(),context);
				}else
				{
					writeLine(" 2+|"+"{attr}"+value+ " |"+v.getComment(), context.currentOutput);
				}
				
			} else
			{
				writeLine(" 3+|"+"{attr}"+value+ " ", context.currentOutput);
			}

		}

	}

	private void writeSubHeader(String name, Context context) throws IOException {
		// Create header for types
		writeTableLine("s",name, "", "", context);
	}

	private void writeTableLine(String prefix,String desc, String obligation, String comment, Context context) throws IOException {
		if(obligation==null)
			obligation="";
		if(comment==null)
			comment="";
		
		writeLine(prefix+"|" + desc + " |" + obligation + " |" + comment,context.currentOutput);
	}

	private void startTable(Context context) throws IOException {
		writeLine(":var: {nbsp}{nbsp}{nbsp}{nbsp}\n"+
		":attr: {var}{var}\n"+
		"[cols=\"25%,10%,65%\",options=\"header\",]\n"+
		"|===",context.currentOutput);
		writeLine("|Description |Obligation |Comment",context.currentOutput);
	}

	private void endTable(Context context) throws IOException {
		writeLine("|===",context.currentOutput);
	}

}
