package fr.ifremer.fr.xsf.generator.model;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GVariableAdapter implements JsonSerializer<Variable>,JsonDeserializer<Variable> {
	private Gson gson;

	public GVariableAdapter()
	{
		gson=new GsonBuilder().registerTypeAdapter(Group.class,new GGroupAdapter())
		.registerTypeAdapter(Type.class,new GTypeAdapter())
		//.registerTypeAdapter(Variable.class,new GVariableAdapter())
		.registerTypeAdapter(Attribute.class,new GAttributeAdapter())
		.setPrettyPrinting().create();
	}
	@Override
	public JsonElement serialize(Variable value, Type type, JsonSerializationContext context) {
		JsonObject object = new JsonObject();
		GBaseAdapter.serialize(object, value, context);
		object.add("dims", context.serialize(value.dims));
		object.add("type", context.serialize(value.type));
		object.add("attributes", context.serialize(value.attributes));
		return object;
		
	}

	@Override
	public Variable deserialize(JsonElement element, Type type, JsonDeserializationContext context)
			throws JsonParseException {
		Variable v=gson.fromJson(element, Variable.class);
		// post deserialization
		v.postRead();
		return v;
	}

	

}
