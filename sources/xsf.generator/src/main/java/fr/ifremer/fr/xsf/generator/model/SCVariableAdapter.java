package fr.ifremer.fr.xsf.generator.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import fr.ifremer.fr.xsf.generator.DataTypeConverter;

public class SCVariableAdapter {

	public static List<String> getSourceDeclaration(Variable variable) {
		return Arrays.asList(
				String.format("\tpublic static final String %s=\"%s\";", variable.name.toUpperCase(), variable.name),
				String.format("\tprivate NCVariable %s;", variable.name));
	}

	public static List<String> getSourceAccessor(Variable variable) {
		return Arrays.asList(String.format(
				"\t/**\n" + "\t * @return the %s\n" + "\t */\n" + "\tpublic NCVariable get%s() throws NCException {\n"
						+ "\t\tif (%s == null) %s;\n" + "\t\treturn %s;" + "\n\t}\n",
				variable.name, CamelCase(variable.name), variable.name, getDeclaratorName(variable), variable.name));
	}

	private static String CamelCase(String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	public static String getDeclaratorName(Variable variable) {
		return String.format("declare_%s()", CamelCase(variable.name));
	}

	public static List<String> declareVariableSourceCode(Variable variable, List<Dimension> env) throws Exception {

		List<String> codeLines = new ArrayList<>();

		codeLines.add(String.format("\n\t/** Variable: %s declarator**/", variable.name));
		codeLines.add(String.format("\n\tprivate void %s throws NCException {\n\t", getDeclaratorName(variable)));

		for (String dim : variable.dims) {
			if (env.stream().noneMatch(d -> d.name.contentEquals(dim))) {
				codeLines.add(String.format("\t\tNCDimension %s = this.getDimension(\"%s\");",
						SCDimensionAdapter.getDimCodeName(dim), dim));
			}
		}

		StringBuilder dimString;
		if (!variable.dims.isEmpty()) {
			dimString = new StringBuilder("Arrays.asList(");
			variable.dims.forEach(d -> dimString.append(SCDimensionAdapter.getDimCodeName(d)
					+ (variable.dims.indexOf(d) == variable.dims.size() - 1 ? ")" : ", ")));
		} else {
			// scalar Variable
			dimString = new StringBuilder("new ArrayList<NCDimension>()");
		}

		if (DataTypeConverter.isVlen(variable.type)) {
			// variable declaration
			codeLines.add(String.format("\t\t%s = addVariable(%s, getType(\"%s\").getValue(), %s);", variable.name,
					variable.name.toUpperCase(), variable.type, dimString));
		} else {
			// variable declaration
			codeLines.add(String.format("\t\t%s = addVariable(%s, %s, %s);", variable.name, variable.name.toUpperCase(),
					DataTypeConverter.getName(variable.type), dimString));
		}

		// attributes declarations
		variable.attributes.forEach(att -> SCAttributeAdapter.getSourceCode(variable, att)
				.forEach(src -> codeLines.add(String.format("\t\t%s.%s", variable.name, src))));

		// add fill value for enum types
		{
			String type = variable.getType();

			if (DataTypeConverter.is_enum(type)) {
				boolean hasFillValue = variable.attributes.stream()
						.anyMatch(att -> att.name.compareTo("_FillValue") == 0);
				if (!hasFillValue) {
					// if we do not have fillValue, fill with default fill value
					// Trick ALL enum are defined as a type byte, we use 127 as defaut value
					String s[] = {};
					Attribute fillValue = new Attribute("_FillValue", "", "", "M", "127", "byte", s);
					SCAttributeAdapter.getSourceCode(variable, fillValue)
							.forEach(src -> codeLines.add(String.format("\t\t%s.%s", variable.name, src)));
				}
			}
		}

		// "comment" attribute
		if (variable.comment != null && !variable.comment.trim().isEmpty()) {
			String[] lines = variable.comment.trim().split("\\n");
			List<String> al = new ArrayList<String>(Arrays.asList(lines));
			String subComments = variable.getSubcomment();
			if (!subComments.trim().isEmpty())
				al.add(subComments);
			if (al.size() > 1) {
				codeLines.add(String.format("\t\t%s.addAttribute(\"comment\", \"%s\\n\" +", variable.name, al.get(0)));

				for (int i = 1; i < al.size() - 1; i++) {
					codeLines.add(String.format("\t\t\t\"%s\\n\" +", al.get(i)));
				}
				codeLines.add(String.format("\t\t\t\"%s\\n\"", al.get(al.size() - 1)));
				codeLines.add("\t\t);");

			} else {
				codeLines.add(
						String.format("\t\t%s.addAttribute(\"comment\", \"%s\");", variable.name, variable.comment));
			}
		}

		// for test purpose add an attribute containing the variable obligation value
		if (variable.obligation != null && !variable.obligation.isEmpty()) {
			codeLines.add(
					String.format("\t\t%s.addAttribute(\"obligation\", \"%s\");", variable.name, variable.obligation));
		}
		codeLines.add("\t}");
		return codeLines;
	}
}
