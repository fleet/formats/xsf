package fr.ifremer.fr.xsf.generator.model;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GGroupAdapter implements JsonSerializer<Group> {

	@Override
	public JsonElement serialize(Group value, Type type, JsonSerializationContext context) {
		JsonObject object = new JsonObject();
		GBaseAdapter.serialize(object, value, context);
		if(value.groupCategory!=null)
			object.add("groupCategory", context.serialize(value.groupCategory));
		object.add("dims", context.serialize(value.dims));
		object.add("types", context.serialize(value.types));
		object.add("attributes", context.serialize(value.attributes));
		object.add("variables", context.serialize(value.variables));
		object.add("coordinatevariables", context.serialize(value.coordinatevariables));
		object.add("subGroups", context.serialize(value.subGroups));
		return object;
		
	}

}
