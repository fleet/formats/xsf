package fr.ifremer.fr.xsf.generator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import fr.ifremer.fr.xsf.generator.model.Attribute;
import fr.ifremer.fr.xsf.generator.model.Group;
import fr.ifremer.fr.xsf.generator.model.Variable;

/**
 * PredefinedLayersGenerator class: generates *.java files where predefined layers of the {@link SounderDataContainer}
 * are defined.
 */
public class PredefinedLayersGenerator {
	public enum DataKind {
		continuous, discrete
	}
	/**
	 * Layer Class: private inner class which contains information about a layer
	 */
	private class Layer {

		public Layer(String groupName, String layerName, String layerClass, String name, String longName,
				String datakind, String unit, String path) {
			super();
			this.groupName = groupName;
			this.layerName = layerName;
			this.layerClass = layerClass;
			this.name = name;
			this.longName = longName;
			this.datakind  = datakind;
			this.unit = unit;
		}

		private static final String CSV_SEPARATOR = ";";

		/** Layer fields **/
		private String groupName;
		private final String layerName;
		private final String layerClass;
		private final String name;
		private final String longName;
		private final String datakind;
		private final String unit;



		/**
		 * Build a layer from a CSV line
		 * 
		 * @throws Exception
		 **/
		public Layer(String l) throws Exception {
			String[] fields = l.split(CSV_SEPARATOR);
			if (fields.length < 6)
				throw new Exception("CSV line format error");
			groupName = fields[0];
			layerName = fields[1];
			layerClass = fields[2];
			name = fields[3];
			longName = fields[4];
			datakind = fields[5];
			unit = fields.length > 6 ? fields[6] : "undefined";
		}

		/** @return declaration code line **/
		public String getDeclaration() {
			String append="";
			if(this.layerClass.contains("String")||this.layerClass.contains("VlenLoadableLayer") )
				 append="//";
			return String.format(
					"\n\t/** Layer: %s **/\n"
					+ "\tpublic static final String %s_VARIABLE_NAME =\"%s\";\n" 
					+"\t"+append+"public static final PredefinedLayers<%s> %s = new PredefinedLayers<>(GROUP,\"%s\",\"%s\",\"%s\", DataKind.%s, %s::new);",
					layerName, layerName,name, layerClass, layerName, name, longName, unit, datakind, layerClass);
		}

		/** @return required import code line **/
		public String getImportLine() {
			String code=String.format("import fr.ifremer.globe.model.sounderdatacontainer.layers.%s;", this.layerClass);
			if( this.layerClass.contains("String") ||this.layerClass.contains("VlenLoadableLayer") )
				return "//"+code;
			return code;
		}


		/** @return group name **/
		public String getGroupName() {
			return groupName;
		}



	}


	//base output directory
	private Path baseOutput;



	public void generateSourceFile(List<Group> groups, Path outputFolder) throws Exception
	{
		// Define output folder path
		this.baseOutput=outputFolder;
		if (!Files.exists(outputFolder))
		{
			Files.createDirectories(outputFolder);
		}

		// Do process
		modelToSourceFiles(groups, outputFolder);
	}

	private void modelToSourceFiles(List<Group> groups, Path outputFolder) throws Exception {
		//create layer objects
		List<Layer> layers=new ArrayList<>();
		String path="";
		groups.forEach(g-> {
			try {
				layers.addAll(parseGroup(g,path));
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		});
		createAllSourceFiles(outputFolder,layers);
	}


	/**
	 * parse group and recurse
	 * @throws Exception 
	 * */
	private List<Layer> parseGroup(Group g,String path) throws Exception
	{
		List<Layer> layers=new ArrayList<>();
		String lPath=path+"/"+g.getName();
		for(Variable v:g.getCoordinateVariables())
		{
			layers.add(parseVariable(g,v,lPath));
		}
		for(Variable v:g.getVariables())
		{
			layers.add(parseVariable(g,v,lPath));
		}

		for(Group subgroup:g.getSubGroups())
		{
			lPath=path+"/"+g.getName();
			layers.addAll(parseGroup(subgroup,lPath));
		}
		return layers;
	}
	/**
	 * parse variables from a group
	 * @throws Exception 
	 * */
	private Layer parseVariable(Group g, Variable v, String lPath) throws Exception {
		String layerName=v.getName().toUpperCase();
		
		//find unit
		String unit="";
		Optional<Attribute> unitValue=findAttribute(v, "units");
		if(unitValue.isPresent())
		{
			unit=unitValue.get().getValue();
		}

		//find className
		String baselayerType="";
		if(v.getType()!=null)
		{
			baselayerType=v.getType();
		} else
		{
			throw new Exception("Variable "+v.getName() + " has not associated type");
		}
		Optional<Attribute> sf=findAttribute(v, "scale_factor");
		String expandedlayerType="";
		if(sf.isPresent())
		{
			expandedlayerType=sf.get().getType();
			if(expandedlayerType==null)
			{
				//assume double scale factor
				expandedlayerType="double";
			}
		} 

		String longName="";
		Optional<Attribute> long_name=findAttribute(v, "long_name");
		if(long_name.isPresent())
		{
			longName=long_name.get().getValue();
			if(long_name.get().getSubcomment()!=null)
				longName+=long_name.get().getSubcomment();
		}
		
		String layerClass=NetcdfLayerDeclarer.getLayerClass(v.getDims().size(),baselayerType, expandedlayerType);
		return new Layer(lPath,layerName, layerClass,v.getName(), longName, DataKind.continuous.toString(), unit, lPath);

	}

	private Optional<Attribute> findAttribute(Variable v,String attributeName)
	{
		for(Attribute att:v.getAttributes())
		{
			if(att.getName().compareToIgnoreCase(attributeName)==0)
			{
				return Optional.of(att);
			}
		}
		return Optional.empty();			
	}

	/** CSV -> .java **/
	@SuppressWarnings("unused")
	private void csvToSourceFiles(Path csvFile, Path outputFolder) throws IOException {
		// Get layers
		List<Layer> layers = getLayersFromCsv(csvFile);
		// Generate .java files
		createAllSourceFiles(outputFolder, layers);
	}


	/**
	 * 
	 * @param csvFilePath input file
	 * @return a layer list from the csv file
	 */
	private List<Layer> getLayersFromCsv(Path csvFilePath) throws IOException {
		List<Layer> result = new ArrayList<>();
		List<String> csvLines = Files.readAllLines(csvFilePath);
		csvLines.remove(0);
		csvLines.forEach(l -> {
			try {
				result.add(new Layer(l));
			} catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
		});
		return result;
	}

	/**
	 * Generates one file by group where predefined layers are declared
	 */
	private void createAllSourceFiles(Path outputFolder, List<Layer> layers) {

		HashMap<String, List<Layer>> layerMap = new HashMap<>();
		layers.forEach(l -> {
			if (!layerMap.containsKey(l.getGroupName()))
				layerMap.put(l.getGroupName(), new ArrayList<Layer>());
			layerMap.get(l.getGroupName()).add(l);
		});

		layerMap.forEach((grp, l) -> {
			try {

				// outputFolder.resolve(grp.)

				generateSourceFile(outputFolder, grp, l);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * Generates a file for a specific group
	 */
	private void generateSourceFile(Path outputFolder, String groupName, List<Layer> layers) throws IOException {

		// Get class name
		String className = new String();

		// Group name
		if(groupName.indexOf("/0") != -1)// case of attitude and position sub group: remove last folder
			groupName = groupName.substring(0, groupName.indexOf("/0"));

		String[] splitedGroupName = groupName.split("/");

		// Output folder
		for (int i = 1; i < splitedGroupName.length - 1 ; i++)
			outputFolder = outputFolder.resolve(splitedGroupName[i]);

		// Class Name
		String[] finalNameArray = splitedGroupName[splitedGroupName.length - 1].split("_");
		for (String s : finalNameArray)
			className += s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
		className += "Layers";

		// Get import and layer declaration lines
		HashSet<String> classToImportSet = new HashSet<>();
		List<String> layerDeclarations = new ArrayList<>();

		classToImportSet.add(String.format("import %s;", "fr.ifremer.globe.model.attributes.Attribute.DataKind"));
		classToImportSet.add(String.format("import %s;", "fr.ifremer.globe.model.sounderdatacontainer.PredefinedLayers"));
		layers.forEach(l -> {
			classToImportSet.add(l.getImportLine());
			layerDeclarations.add(l.getDeclaration());
		});
		List<String> classToImport = new ArrayList<>(classToImportSet);

		Collections.sort(layerDeclarations);
		Collections.sort(classToImport);

		// Build file contents
		List<String> fileContent = new ArrayList<>();

		// Package name
		Path o=outputFolder.subpath(this.baseOutput.getNameCount()-1, outputFolder.getNameCount());

		String[] splitedPath = StreamSupport.stream(o.spliterator(), false).map(Path::toString)
				.toArray(String[]::new);
		String relativePackage="";
		for (String s : splitedPath) {
			relativePackage=relativePackage+"."+s;
		}
		//little hack while root group exist
		relativePackage=relativePackage.replace("root.", "");

		fileContent.add("package fr.ifremer.globe.model.sounderdatacontainer" + relativePackage.toLowerCase() + ";\n");

		classToImport.forEach(importLine -> fileContent.add(importLine));

		fileContent.add(String.format("\n/** Class generated by %s **/", this.getClass().getSimpleName()));
		fileContent.add(String.format("public class %s {", className));

		fileContent.add(String.format("\n\tprivate %s() {}", className));

		fileContent.add(String.format("\n\tpublic static final String GROUP = \"%s\";", groupName));

		layerDeclarations.forEach(declarationLine -> {
			fileContent.add(declarationLine);
		});

		fileContent.add("}");

		if (!Files.exists(outputFolder))
			Files.createDirectories(outputFolder);

		// Write file
		Files.write(outputFolder.resolve(Paths.get(String.format("%s.java", className))), fileContent);

	}

}
