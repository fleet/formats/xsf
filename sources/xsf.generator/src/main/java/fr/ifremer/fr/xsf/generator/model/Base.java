package fr.ifremer.fr.xsf.generator.model;

public class Base {
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	
	public String getComment() {
		if(comment==null)
			return "";
		//allow multiline comments with +
//		return comment.replace("+", "+\r\n");
		return comment;
	}

	public String getObligation() {
		return obligation;
	}
	public String getSubcomment() {
		if(subcomment==null)
			return "";
		return subcomment;
	}
	protected final String name;
	protected final String description;
	protected final String comment;
	protected final String subcomment;
	protected final String obligation;

	public Base(String name, String description, String comment, String obligation) {
		super();
		if (name == null || name.trim().isEmpty()) {
			this.name = null;
		} else {
			this.name = name;
		}
		if (description == null || description.trim().isEmpty()) {
			this.description = null;
		} else {
			this.description = null;
//			this.description=null;
		}
		if (comment == null || comment.trim().isEmpty()) {
			this.comment = null;
		} else {
			this.comment = comment;
		}
		if (obligation == null || obligation.trim().isEmpty()) {
			this.obligation = null;
		} else {

			this.obligation = obligation;
		}
		subcomment="";
	}

	

}
