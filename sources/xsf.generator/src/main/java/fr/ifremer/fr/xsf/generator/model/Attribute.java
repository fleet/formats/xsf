package fr.ifremer.fr.xsf.generator.model;

public class Attribute extends Base{
	final String value;
    private final String type;
	final String[] values;

	public Attribute(String name, String description, String comment, String obligation, String value, String type,String[] values) {
		super(name, description, comment, obligation);
		if(value==null || value.trim().isEmpty())
			this.value=null;
			else
		this.value = value;
		if(type==null || type.trim().isEmpty())
		{
			this.type=null;
		} else
		{
			this.type = type;
			
		}
		if(values==null || values.length==0)
			this.values=null;
		else
			this.values=values;
	}

	public String[] getValues() {
		return values;
	}

	public String getValue() {
		return value;
	}

	public String getType() {
		return type;
	}

}
