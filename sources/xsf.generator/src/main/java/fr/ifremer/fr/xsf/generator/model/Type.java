package fr.ifremer.fr.xsf.generator.model;

public class Type extends Base {

	public Type(String name, String description, String comment, String obligation, String xsfOverride) {
		super(name, description, comment, obligation);
	}

}
