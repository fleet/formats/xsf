package fr.ifremer.fr.xsf.generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import fr.ifremer.fr.xsf.generator.model.Attribute;
import fr.ifremer.fr.xsf.generator.model.Dimension;
import fr.ifremer.fr.xsf.generator.model.Group;
import fr.ifremer.fr.xsf.generator.model.Type;
import fr.ifremer.fr.xsf.generator.model.Variable;


public class ExcelGenerator {
	/**
	 * generate documentation in excel file format
	 **/
	public void run(Group root, File output) throws IOException
	{
		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

		ExcelContext context=new ExcelContext( workbook);
		writeGroup("",root, context);
		// Write the output to a file
		FileOutputStream fileOut = new FileOutputStream(output);
		workbook.write(fileOut);
		fileOut.close();

		// Closing the workbook
	}

	private void writeGroup(String path, Group group,ExcelContext context)
	{
		// Create a Sheet
		Sheet sheet = context.workbook.createSheet(path+"#"+group.getName());
		createHeader(sheet, context);
		writeTypes(sheet,group.getTypes(),context);
		writeDimensions(sheet,group.getDims(),context);
		writeAttributes(sheet,group.getAttributes(),context);
		writeVariables(sheet,group.getVariables(),context);
		for(Group subgroup:group.getSubGroups())
		{
			writeGroup(path+"#"+group.getName(), subgroup, context);
		}
		// Resize all columns to fit the content size
		for(int i = 0; i < context.header.length; i++) {
			sheet.setColumnWidth(i, context.colSize[i]);
		}
	}
	private void writeTypes(Sheet sheet, List<Type> types, ExcelContext context) {
		// Create header for types

		writeSubHeader(sheet, "Types", context);
		//write types
		for(Type v:types)
		{
			Row headerRow = sheet.createRow(context.getLastRowCounterAndIncrement(sheet));
			Cell cell = headerRow.createCell(HEADER.DESCRIPTION.getValue());
			String value = v.getName();
			setInnerCellValue(cell,context,formatString(value));

			cell = headerRow.createCell(HEADER.OBLIGATION.getValue());
			setInnerCellValue(cell,context,formatString(v.getObligation()));

			cell = headerRow.createCell(HEADER.COMMENT.getValue());
			setInnerCellValue(cell,context,formatString(v.getComment()));

			addEmptyLine(sheet,context);

		}
		addEmptyLine(sheet,context);
	}

	private void writeSubHeader(Sheet sheet, String name, ExcelContext context)
	{
		// Create header for types
		Row headerRow = sheet.createRow(context.getLastRowCounterAndIncrement(sheet));
		Cell cell = headerRow.createCell(HEADER.DESCRIPTION.getValue());
		setInnerCellValue(cell,context,formatString(name));
		cell.setCellStyle(context.headerCellStyle);
		headerRow.createCell(HEADER.OBLIGATION.getValue()).setCellStyle(context.headerCellStyle);
		headerRow.createCell(HEADER.COMMENT.getValue()).setCellStyle(context.headerCellStyle);

	}


	private void addEmptyLine(Sheet sheet, ExcelContext context)
	{
		sheet.createRow(context.getLastRowCounterAndIncrement(sheet));
	}
	private void writeVariables(Sheet sheet, List<Variable> variables, ExcelContext context) {

		writeSubHeader(sheet, "Variables", context);

		for(Variable v:variables)
		{
			Row headerRow = sheet.createRow(context.getLastRowCounterAndIncrement(sheet));
			Cell cell = headerRow.createCell(HEADER.DESCRIPTION.getValue());
			String value = v.getType()!=null ? v.getType():"";
			value+=":";
			value+=v.getName();
			String dims=v.getDims().toString();
			value+=dims.replace('[','(').replace(']', ')');
			setInnerCellValue(cell,context,formatString(value));

			cell = headerRow.createCell(HEADER.OBLIGATION.getValue());
			setInnerCellValue(cell,context,formatString(v.getObligation()));

			cell = headerRow.createCell(HEADER.COMMENT.getValue());
			setInnerCellValue(cell,context,formatString(v.getComment()));

			writeVariableAttributes(sheet,v.getAttributes(),context);
			addEmptyLine(sheet,context);

		}
		addEmptyLine(sheet,context);
	}

	private void writeVariableAttributes(Sheet sheet, List<Attribute> attributes, ExcelContext context) {
		for(Attribute v:attributes)
		{
			Row headerRow = sheet.createRow(context.getLastRowCounterAndIncrement(sheet));
			Cell cell = headerRow.createCell(HEADER.DESCRIPTION.getValue());
			String value = v.getType()!=null ? v.getType():"";
			value+=":";
			value+=v.getName();
			//assume a string type
			if(v.getType()==null|| v.getType().compareToIgnoreCase("string")==0)
			{
				if(v.getValue()!=null)
				{
					value+="=\"";
					value+=v.getValue();
					value+="\"";
				}
			} else
			{
				if(v.getValue()!=null)
				{
					value+="=";
					value+=v.getValue();
				}
			}
			setInnerCellValue(cell,context,formatString(value));
			cell = headerRow.createCell(HEADER.OBLIGATION.getValue());
			setInnerCellValue(cell,context,formatString(v.getObligation()));

			cell = headerRow.createCell(HEADER.COMMENT.getValue());
			setInnerCellValue(cell,context,formatString(v.getComment()));

		}

	}

	private void writeAttributes(Sheet sheet, List<Attribute> attributes, ExcelContext context) {
		writeSubHeader(sheet, "Attributes", context);
		for(Attribute v:attributes)
		{
			Row headerRow = sheet.createRow(context.getLastRowCounterAndIncrement(sheet));
			Cell cell = headerRow.createCell(HEADER.DESCRIPTION.getValue());
			String value = v.getType()!=null ? v.getType():"";
			value+=":";
			value+=v.getName();
			setInnerCellValue(cell,context,formatString(value));

			cell = headerRow.createCell(HEADER.OBLIGATION.getValue());
			setInnerCellValue(cell,context,formatString(v.getObligation()));

			cell = headerRow.createCell(HEADER.COMMENT.getValue());
			setInnerCellValue(cell,context,formatString(v.getComment()));
		}
		addEmptyLine(sheet,context);

	}

	private void writeDimensions(Sheet sheet, List<Dimension> dims, ExcelContext context) {
		writeSubHeader(sheet, "Dimensions", context);
		for(Dimension v:dims)
		{
			Row headerRow = sheet.createRow(context.getLastRowCounterAndIncrement(sheet));
			Cell cell = headerRow.createCell(HEADER.DESCRIPTION.getValue());
			String value = v.getName();
			value+=":";
			value+=v.getName();
			setInnerCellValue(cell,context,formatString(value));

			cell = headerRow.createCell(HEADER.OBLIGATION.getValue());
			setInnerCellValue(cell,context,formatString(v.getObligation()));

			cell = headerRow.createCell(HEADER.COMMENT.getValue());
			setInnerCellValue(cell,context,formatString(v.getComment()));
//			cell.getCellStyle().setWrapText(true);
		}
		addEmptyLine(sheet,context);


	}
	
	public static void setInnerCellValue(Cell cell, ExcelContext context, String value)
	{
		cell.setCellStyle(context.innerCellStyle);
		cell.setCellValue(value);
	}
	public static String formatString(String value)
	{
		if(value==null)
			return "";
		return value.replace("\\n", System.lineSeparator());
	}
	class ExcelContext
	{
		CellStyle headerCellStyle;
		CellStyle subHeaderCellStyle;
		CellStyle innerCellStyle;
		Workbook workbook;
		final String header[]= {"Description","Obligation","Comment"};
		final int colSize[]= {255*60,255*20,255*100};
		public ExcelContext(Workbook workbook) {
			super();
			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.BLACK.getIndex());

			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
			this.headerCellStyle = headerCellStyle;
			
			subHeaderCellStyle=headerCellStyle;
			innerCellStyle = workbook.createCellStyle();
			innerCellStyle.setWrapText(true);
			this.workbook = workbook;
		}
		HashMap<Sheet, Integer> rowCounter=new HashMap<>();

		int getLastRowCounterAndIncrement(Sheet sheet)
		{
			if(rowCounter.containsKey(sheet))
			{
				int ret=rowCounter.get(sheet)+1;
				rowCounter.put(sheet, ret);
				return ret;
			}else
			{
				rowCounter.put(sheet,0);
				return 0;
			}
		}
	}
	enum HEADER {
		DESCRIPTION(0),
		OBLIGATION(1),
		COMMENT(2);

		HEADER(int v)
		{
			value=v;
		}
		public int getValue()
		{
			return value;
		}
		int value;
	}
	public void createHeader(Sheet sheet , ExcelContext context)
	{
		// Create a Row
		Row headerRow = sheet.createRow(context.getLastRowCounterAndIncrement(sheet));
		for(int i=0;i<context.header.length;i++)
		{
			Cell cell = headerRow.createCell(i);
			setInnerCellValue(cell,context,formatString(context.header[i]));
			cell.setCellStyle(context.headerCellStyle);
		}
	}


}
