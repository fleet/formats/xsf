package fr.ifremer.fr.xsf.generator.model;

public class Dimension extends Base {
	public final String value;
	public final String javaValue;
	
	public Dimension(String name, String description, String comment, String obligation,String value, String javaValue) {
		super(name, description, comment, obligation);
		this.value=value;
		this.javaValue = javaValue;
	}
}
