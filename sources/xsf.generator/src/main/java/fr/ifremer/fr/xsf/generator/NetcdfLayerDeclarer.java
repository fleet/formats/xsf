package fr.ifremer.fr.xsf.generator;

public class NetcdfLayerDeclarer {
	public static String getLayerClass(int dim, String basetype, String expandedlayerType) throws Exception
	{
		if(DataTypeConverter.isVlen(basetype))
		{
			DataType baseType;
			if(expandedlayerType.isEmpty())
				baseType=DataTypeConverter.getVlenDataType(basetype);
			else
				baseType=DataTypeConverter.get(expandedlayerType);
			return getVlenLayerClass(dim, baseType);
		}
		else 
		{
			String type=expandedlayerType.isEmpty() ? basetype:expandedlayerType;
			if(DataTypeConverter.is_enum(type))
				return getClassicLayerClass(dim, DataType.BYTE);
			return getClassicLayerClass(dim, DataTypeConverter.get(type));
		}
		
	}
	static String getClassicLayerClass(int dim, DataType type)
	{
		if (dim == 0) dim=1;
		switch (type) {
		
		case BOOLEAN:
			return "BooleanLoadableLayer"+dim+"D";
		case BYTE:
			return "ByteLoadableLayer"+dim+"D";
		case CHAR:
			return "ShortLoadableLayer"+dim+"D";
		case UBYTE:
		case SHORT:
			return "ShortLoadableLayer"+dim+"D";
		case USHORT:
		case INT:
			return "IntLoadableLayer"+dim+"D";
		case UINT:
		case LONG:
		case ULONG:
			return "LongLoadableLayer"+dim+"D";
		case FLOAT:
			return "FloatLoadableLayer"+dim+"D";
		case DOUBLE:
			return "DoubleLoadableLayer"+dim+"D";
		case STRING:
			return "StringLoadableLayer"+dim+"D";
		default:
			return null;
		}
	}
	static String getVlenLayerClass(int dim, DataType type)
	{
		if (dim == 0) dim=1;
		switch (type) {
		case BOOLEAN:
			return "BooleanVlenLoadableLayer"+dim+"D";
		case BYTE:
			return "ByteVlenLoadableLayer"+dim+"D";
		case CHAR:
			return "ShortVlenLoadableLayer"+dim+"D";
		case UBYTE:
		case SHORT:
			return "ShortVlenLoadableLayer"+dim+"D";
		case USHORT:
		case INT:
			return "IntVlenLoadableLayer"+dim+"D";
		case UINT:
		case LONG:
		case ULONG:
		case UINT64:
			return "LongVlenLoadableLayer"+dim+"D";
		case FLOAT:
			return "FloatVlenLoadableLayer"+dim+"D";
		case DOUBLE:
			return "DoubleVlenLoadableLayer"+dim+"D";
		default:
			return null;
		}
	}
}
