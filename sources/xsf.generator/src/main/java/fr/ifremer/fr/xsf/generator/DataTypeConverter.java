package fr.ifremer.fr.xsf.generator;

import java.util.HashMap;
import java.util.List;

public class DataTypeConverter {
	private static HashMap<String , DataType > vlen_specific_types=new HashMap<>();
	
	public static String getName(String value) throws Exception
	{
		switch(value.toUpperCase())
		{
			case "TRANSDUCER_TYPE_T":
			case "DETECTION_TYPE_T":
			case "TRANSMIT_T":
			case "BEAM_STABILISATION_T":
			case "BACKSCATTER_TYPE_T":
			case "RANGE_AXIS_INTERVAL_TYPE_T":
			case "PING_AXIS_INTERVAL_TYPE_T":
			case "PING_T":
			case "BEAM_T":
				return "getEnum(\""+value+"\").getValue()";
			default:
				return "DataType."+get(value).name();
		}
	}	
	
	public static DataType get(String value) throws Exception
	{
		switch(value.toUpperCase())
		{
			case "BOOLEAN":
				return DataType.BOOLEAN;
			case "BYTE":
				return DataType.BYTE;
			case "CHAR":
				return DataType.SHORT;
			case "UBYTE":
				return DataType.UBYTE;
			case "SHORT":
				return DataType.SHORT;
			case "USHORT":
				return DataType.USHORT;
			case "INT":
				return DataType.INT;
			case "UINT":
				return DataType.UINT;
			case "LONG":
				return DataType.LONG;
			case "UINT64":
				return DataType.ULONG;
			case "ULONG":
				return DataType.ULONG;
			case "FLOAT":
				return DataType.FLOAT;
			case "DOUBLE":
				return DataType.DOUBLE;
			case "STRING":
				return DataType.STRING;
			default: 
		}
		throw new Exception("Unknown netcdf type " + value);
	}
	/**
	 * Check if a type is a user defined type
	 * */
	public static boolean isVlen(String name)
	{
		return vlen_specific_types.containsKey(name);
	}
	
	public static boolean is_enum(String name)
	{
		return name.endsWith("_t") && !isVlen(name);
	}
	
	/**
	 * declare a specific type
	 * @param name the name of the type
	 * @param declaration the declaration type (in a float* form)
	 * @return the name of the type
	 * */
	public static String declareVlenType(String declaration)
	{
		//parse declaration
		//ignore enum type for now
		if(declaration.contains("enum"))
			return declaration;
		//we are in a float* case
		String[] decode=declaration.split(" ");
		String name=decode[1];
		String typeName=decode[0].replace("*", "").replace("(", "").replace(")","");
		
		//retrieve base type
		DataType type=DataType.getType(typeName);
		vlen_specific_types.put(name,type);
		return name;
	}
	
	public static DataType getVlenDataType(String key)
	{
		return vlen_specific_types.get(key);
	}
}
