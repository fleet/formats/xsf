package fr.ifremer.fr.xsf.generator.model;

import java.util.ArrayList;
import java.util.List;

enum GroupCategory{
	eStandard,eSensor,eMultipleInstance
}

public class Group extends Base{
	GroupCategory groupCategory = GroupCategory.eStandard;
	List<Attribute> attributes = new ArrayList<>();
	List<Type> types = new ArrayList<>();
	List<Dimension> dims = new ArrayList<>();
	List<Variable> variables = new ArrayList<>();
	List<Variable> coordinatevariables = new ArrayList<>();

	public GroupCategory getGroupCategory() {
		if (groupCategory==null) return GroupCategory.eStandard;  //deserialization could lead to null values in this variables
		return groupCategory;
	}
	protected List<Group> subGroups = new ArrayList<>();

	public Group(String name, String description, String comment, String obligation) {
		super(name, description, comment, obligation);
	}
	public List<Attribute> getAttributes() {
		return attributes;
	}
	
	public List<Type> getTypes() {
		return types;
	}

	public List<Dimension> getDims() {
		return dims;
	}

	public List<Variable> getVariables() {
		return variables;
	}
	public List<Variable> getCoordinateVariables() {
		//happens in case of deserialization
		if(coordinatevariables==null)
			coordinatevariables=new ArrayList<Variable>();
		return coordinatevariables;
	}
	public List<Group> getSubGroups() {
		return subGroups;
	}
}
