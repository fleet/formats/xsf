/*
 * Copyright 1998-2014 University Corporation for Atmospheric Research/Unidata
 *
 *   Portions of this software were developed by the Unidata Program at the
 *   University Corporation for Atmospheric Research.
 *
 *   Access and use of this software shall impose the following obligations
 *   and understandings on the user. The user is granted the right, without
 *   any fee or cost, to use, copy, modify, alter, enhance and distribute
 *   this software, and any derivative works thereof, and its supporting
 *   documentation for any purpose whatsoever, provided that this entire
 *   notice appears in all copies of the software, derivative works and
 *   supporting documentation.  Further, UCAR requests that the user credit
 *   UCAR/Unidata in any publications that result from the use of this
 *   software or in any product that includes this software. The names UCAR
 *   and/or Unidata, however, may not be used in any advertising or publicity
 *   to endorse or promote any products or commercial entity unless specific
 *   written permission is obtained from UCAR/Unidata. The user also
 *   understands that UCAR/Unidata is not obligated to provide the user with
 *   any support, consulting, training or assistance of any kind with regard
 *   to the use, operation and performance of this software nor to provide
 *   the user with any updates, revisions, new versions or "bug fixes."
 *
 *   THIS SOFTWARE IS PROVIDED BY UCAR/UNIDATA "AS IS" AND ANY EXPRESS OR
 *   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL UCAR/UNIDATA BE LIABLE FOR ANY SPECIAL,
 *   INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
 *   FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 *   NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 *   WITH THE ACCESS, USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package fr.ifremer.fr.xsf.generator;

import java.math.BigInteger;

/**
 * This class is a mixed from the ones extracted in jna Ncprototypes.java class
 * and netcdf java api DataType.java
 */
////NC_types, from Nc4prototypes 
interface Nc4prototypes {
	static int NC_NAT = 0; /* Not-A-Type */
	static int NC_BYTE = 1; /* signed 1 byte integer */
	static int NC_CHAR = 2; /* ISO/ASCII character */
	static int NC_SHORT = 3; /* signed 2 byte integer */
	static int NC_INT = 4; /* signed 4 byte integer */
	static int NC_FLOAT = 5; /* single precision floating point number */
	static int NC_DOUBLE = 6; /* double precision floating point number */
	static int NC_UBYTE = 7; /* unsigned 1 byte int */
	static int NC_USHORT = 8; /* unsigned 2-byte int */
	static int NC_UINT = 9; /* unsigned 4-byte int */
	static int NC_INT64 = 10; /* signed 8-byte int */
	static int NC_UINT64 = 11;/* unsigned 8-byte int */
	static int NC_STRING = 12; /* string */
	static int NC_MAX_ATOMIC_TYPE = NC_STRING;
	/////
}

public enum DataType {

	BOOLEAN("boolean", 1, boolean.class, false, Nc4prototypes.NC_BYTE, "NC_BYTE"),
	BYTE("byte", 1, byte.class, false, Nc4prototypes.NC_BYTE, "NC_BYTE"),
	CHAR("char", 1, char.class, false, Nc4prototypes.NC_CHAR, "NC_CHAR"),
	SHORT("short", 2, short.class, false, Nc4prototypes.NC_SHORT, "NC_SHORT"),
	INT("int", 4, int.class, false, Nc4prototypes.NC_INT, "NC_INT"),
	LONG("long", 8, long.class, false, Nc4prototypes.NC_INT64, "NC_INT64"),
	FLOAT("float", 4, float.class, false, Nc4prototypes.NC_FLOAT, "NC_FLOAT"),
	DOUBLE("double", 8, double.class, false, Nc4prototypes.NC_DOUBLE, "NC_DOUBLE"),
	// object types
	STRING("String", 4, String.class, false, Nc4prototypes.NC_STRING, "NC_STRING"), // 32-bit index
	ENUM1("enum1", 1, byte.class, false, Nc4prototypes.NC_BYTE, "NC_BYTE"), // byte
	ENUM2("enum2", 2, short.class, false, Nc4prototypes.NC_SHORT, "NC_SHORT"), // short
	ENUM4("enum4", 4, int.class, false, Nc4prototypes.NC_INT, "NC_INT"), // int
//	OPAQUE("opaque", 1, ByteBuffer.class, false), // byte blobs
//	OBJECT("object", 1, Object.class, false,), // added for use with Array
	UBYTE("ubyte", 1, byte.class, true, Nc4prototypes.NC_UBYTE, "NC_UBYTE"),
	USHORT("ushort", 2, short.class, true, Nc4prototypes.NC_USHORT, "NC_USHORT"),
	UINT("uint", 4, int.class, true, Nc4prototypes.NC_UINT, "NC_UINT"),
	ULONG("ulong", 8, long.class, true, Nc4prototypes.NC_UINT64, "NC_UINT64"),
	UINT64("uint64", 8, long.class, true, Nc4prototypes.NC_UINT64, "NC_UINT64");

	private final String niceName;
	private final int size;
	private final Class<?> primitiveClass;
	private final boolean isUnsigned;
	private int nc_type;
	private String nc_type_name;

	/**
	 * Return the native nc_type associated with this enum
	 */
	public int getNc_type() {
		return nc_type;
	}

	DataType(String s, int size, Class<?> primitiveClass, boolean isUnsigned, int nc_type, String nc_type_name) {
		this.niceName = s;
		this.size = size;
		this.primitiveClass = primitiveClass;
		this.isUnsigned = isUnsigned;
		this.nc_type = nc_type;
		this.nc_type_name = nc_type_name;
	}

	public String getNc_type_name() {
		return nc_type_name;
	}

	/**
	 * The DataType name, eg "byte", "float", "String".
	 */
	public String toString() {
		return niceName;
	}

	/**
	 * Size in bytes of one element of this data type. Strings dont know, so return
	 * 0. Structures return 1.
	 *
	 * @return Size in bytes of one element of this data type.
	 */
	public int getSize() {
		return size;
	}

	/**
	 * The primitive class type: char, byte, float, double, short, int, long,
	 * boolean, String, StructureData, StructureDataIterator, ByteBuffer.
	 *
	 * @return the primitive class type
	 */
	public Class<?> getPrimitiveClassType() {
		return primitiveClass;
	}

	public boolean isUnsigned() {
		return isUnsigned;
	}

	/**
	 * Is String or Char
	 *
	 * @return true if String or Char
	 */
	public boolean isString() {
		return (this == DataType.STRING) || (this == DataType.CHAR);
	}

	/**
	 * Is Byte, Float, Double, Int, Short, or Long
	 *
	 * @return true if numeric
	 */
	public boolean isNumeric() {
		return (this == DataType.FLOAT) || (this == DataType.DOUBLE) || isIntegral();
	}

	/**
	 * Is Byte, Int, Short, or Long
	 *
	 * @return true if integral
	 */
	public boolean isIntegral() {
		return (this == DataType.BYTE) || (this == DataType.INT) || (this == DataType.SHORT) || (this == DataType.LONG)
				|| (this == DataType.UBYTE) || (this == DataType.UINT) || (this == DataType.USHORT)
				|| (this == DataType.ULONG);
	}

	/**
	 * Is Float or Double
	 *
	 * @return true if floating point type
	 */
	public boolean isFloatingPoint() {
		return (this == DataType.FLOAT) || (this == DataType.DOUBLE);
	}

	/**
	 * Is this an enumeration types?
	 *
	 * @return true if ENUM1, 2, or 4
	 */
	public boolean isEnum() {
		return (this == DataType.ENUM1) || (this == DataType.ENUM2) || (this == DataType.ENUM4);
	}

	public boolean isEnumCompatible(DataType inferred) {
		if (inferred == null)
			return false;
		if (this == inferred)
			return true;
		switch (this) {
		case ENUM1:
			return inferred == DataType.BYTE || inferred == DataType.STRING;
		case ENUM2:
			return inferred == DataType.SHORT || inferred == DataType.STRING;
		case ENUM4:
			return inferred == DataType.INT || inferred == DataType.STRING;
		default:
			break;
		}
		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////

	public static DataType enumTypeize(DataType dt) {
		switch (dt) {
		case BYTE:
		case UBYTE:
			return ENUM1;
		case SHORT:
		case USHORT:
			return ENUM2;
		case INT:
		case UINT:
			return ENUM4;
		default:
			break;
		}
		return dt;
	}

	/**
	 * Find the DataType that matches this name.
	 *
	 * @param name find DataType with this name.
	 * @return DataType or null if no match.
	 */
	public static DataType getType(String name) {
		if (name == null)
			return null;
		try {
			return valueOf(name.toUpperCase());
		} catch (IllegalArgumentException e) { // lame!
			return null;
		}
	}

	/**
	 * Find the DataType that matches this class.
	 *
	 * @param c primitive or object class, eg float.class or Float.class
	 * @return DataType or null if no match.
	 */
	public static DataType getType(Class<?> c, boolean isUnsigned) {
		if ((c == float.class) || (c == Float.class))
			return DataType.FLOAT;
		if ((c == double.class) || (c == Double.class))
			return DataType.DOUBLE;
		if ((c == short.class) || (c == Short.class))
			return isUnsigned ? DataType.USHORT : DataType.SHORT;
		if ((c == int.class) || (c == Integer.class))
			return isUnsigned ? DataType.UINT : DataType.INT;
		if ((c == byte.class) || (c == Byte.class))
			return isUnsigned ? DataType.UBYTE : DataType.BYTE;
		if ((c == char.class) || (c == Character.class))
			return DataType.CHAR;
		if ((c == boolean.class) || (c == Boolean.class))
			return DataType.BOOLEAN;
		if ((c == long.class) || (c == Long.class))
			return isUnsigned ? DataType.ULONG : DataType.LONG;
		if (c == String.class)
			return DataType.STRING;

		throw new RuntimeException("Unknown or unsupported type");
	}

	/**
	 * convert an unsigned long to a String
	 *
	 * @param li unsigned int
	 * @return equivilent long value
	 */
	public static String unsignedLongToString(long li) {
		if (li >= 0)
			return Long.toString(li);

		// else do the hard part - see
		// http://technologicaloddity.com/2010/09/22/biginteger-as-unsigned-long-in-java/
		byte[] val = new byte[8];
		for (int i = 0; i < 8; i++) {
			val[7 - i] = (byte) ((li) & 0xFF);
			li = li >>> 8;
		}

		BigInteger biggy = new BigInteger(1, val);
		return biggy.toString();
	}

	/**
	 * widen an unsigned int to a long
	 *
	 * @param i unsigned int
	 * @return equivilent long value
	 */
	public static long unsignedIntToLong(int i) {
		return (i < 0) ? (long) i + 4294967296L : (long) i;
	}

	/**
	 * widen an unsigned short to an int
	 *
	 * @param s unsigned short
	 * @return equivilent int value
	 */
	public static int unsignedShortToInt(short s) {
		return (s & 0xffff);
	}

	/**
	 * widen an unsigned byte to a short
	 *
	 * @param b unsigned byte
	 * @return equivilent short value
	 */
	public static short unsignedByteToShort(byte b) {
		return (short) (b & 0xff);
	}

}
