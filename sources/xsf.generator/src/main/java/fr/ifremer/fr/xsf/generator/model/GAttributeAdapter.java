package fr.ifremer.fr.xsf.generator.model;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GAttributeAdapter implements JsonSerializer<Attribute> {

	@Override
	public JsonElement serialize(Attribute value, Type type, JsonSerializationContext context) {
		JsonObject object = new JsonObject();
		GBaseAdapter.serialize(object, value, context);
		object.add("value", context.serialize(value.getValue()));
		object.add("type", context.serialize(value.getType()));
		object.add("values", context.serialize(value.getValues()));
		return object;
		
	}

}
